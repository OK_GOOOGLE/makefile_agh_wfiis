# README #


### What is this repository for? ###
This is makefile for C/ C++ especially for students from AGH WFiIS


If you find something difficult, you always can check it out in the documentation 
https://www.gnu.org/software/make/manual/make.html

### How do I get set up? ###
1. make folder for your classes
2. type: git init
3. type: git remote add origin git@bitbucket.org:UserName/RepoName.git
4. type: git config --global core.editor vim 
5. prepare your remote git repo:

   > login to bitbucket

   > send ssh key

   > make repo for your classes

6. make folder for your new exercise
7. copy makefile to that folder
8. keep your code with makefile

/////////////////////////////////////////////////////////////

   make             - kompilacja i linkowanie projektu   
   make help        - wyświetla ten komunikat oraz krótki opis            
   make clean       - czyszczenie projektu               
   make run         - uruchamia program                  
   make gdb         - uruchamia gdb                      
   make valgrind    - uruchamia valgrind  
   make git         - wysyła zmiany na zdalne repo  
   make update      - pobiera najnowszą wersję makefile
     
/////////////////////////////////////////////////////////////

### Who do I talk to? ###

 Repo owner or admin

 Other community or team contact

### Contribute ###
I encourage everyone to get involved into the Project. It's fine for the regular use for classes or for private exercises, but everything can be improved ;) 
If you have any new ideas please put them into the issues section.

If you would like to solve some issues pleas do ;) if you don't know how to commit your changes here is short tutorial on how to do that
https://youtu.be/FQsBmnZvBdc